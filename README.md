### agw

A pyautogui wrapper library for data entry macros.


## Wrapper

The Automater class wraps up and organizes the pyautogui functions a little more to my liking:

    from agw import Automator

    ai = Automator()

    ai.cursor.to(10, 10)
    ai.cursor.click()

    ai.keyboard.write('a wee bit of data input')
    ai.keyboard.enter()


### Application

The AutomatorApp class provides a convenience method of bundling a number of macro
function into a single command line call:

    import agw
    from agw.app import AutomatorApp
    from agw.data import MacroDataFileHandler, iterfile


    def macro_1(ai):
        "Test Macro #1"
        result = ai.msgbox.confirm("Testing Macro #1")
        print(result)


    def macro_2(ai):
        "Test Macro #2"
        result = ai.msgbox.confirm("Testing Macro #2")
        print(result)


    @iterfile("name id")
    def macro_3(ai, data):
        "Test Macro #3 - with data"
        ai.get_data_handling_inputs()
        print("")
        for index, line in data:
            print(f"   {index:0>3} {line.name} {line.id}")


    app = AutomatorApp()

    app += macro_1
    app += macro_2
    app += macro_3

    app.run()


Output looks like this:

    ... python test/run_example.py --data-file test/sample_data.txt

    [ ] Test Macro #1
    [ ] Test Macro #2
    [x] Test Macro #3 - with data

    Running Test Macro #3 - with data macro...
      Throttle input (0.1-2.0): 1
      Pause after every N lines:  7
      Start from line:  1

       001 Sample A 1224
       002 Sample B 33456
       003 Sample C 55667
    ...
