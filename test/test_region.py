from agw import screen


def test_region():

    r = screen.Region(10, 20, 100, 100)

    assert r.x == 10
    assert r.y == 20
    assert r.w == 90
    assert r.h == 80
    assert r.x1 == 100
    assert r.y1 == 100

    r = screen.Region(10, 20, w=100, h=100)

    assert r.x == 10
    assert r.y == 20
    assert r.x1 == 110
    assert r.y1 == 120
    assert r.w == 100 
    assert r.h == 100 

    assert r.box[0] == 10
    assert r.box[1] == 20
    assert r.box[2] == 100
    assert r.box[3] == 100

    assert r.box == screen.Region.from_coordinates(10, 20, 110, 120).box
    assert r.box == screen .Region.from_measurements(10, 20, 100, 100).box
