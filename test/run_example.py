from agw.app import AutomatorApp
from agw.data import iterfile


def macro_1(ai):
    "Test Macro #1"
    result = ai.msgbox.confirm("Testing Macro #1")
    print(result)


def macro_2(ai):
    "Test Macro #2"
    result = ai.msgbox.confirm("Testing Macro #2")
    print(result)


@iterfile("name id")
def macro_3(ai, data):
    "Test Macro #3 - with data"
    ai.get_data_handling_inputs()
    print("")
    for index, line in data:
        print(f"   {index:0>3} {line.name} {line.id}")


app = AutomatorApp()

app += macro_1
app += macro_2
app += macro_3

app.display_available_macros()
app.run()
