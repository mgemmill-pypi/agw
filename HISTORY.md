#### agw - a pyautogui wrapper


##### v0.4.4

- Allow to pass a None `data_fields` argument to iterfile decorator.
  Results will be plain list objects, rather than a namedtuple.


##### v0.4.3

- added traceback to general error handler in App.run call.


##### v0.4.2

- fix bug in Region object - needed more variation in test data!


##### v0.4.1

- added Region object to handle working with screen areas
  by either x/y and x1/y1 coordinates or x/y and w/h


##### v0.4.0

- added more convenience keyboard methods
- fixed spelling of Automator class
- fixed bug in Keyboard.f method
- added named cursor position functionality


##### v0.3.0

- added agw cli utility commands:
    - cursor command for tracking cursor position.
    - jiggler command for keeping screen alive.
    - screen command for taking desktop images and find images on desktop.


##### v0.2.0

- added AutomatorApp cli
- added MacroDataFileHandler for common CSV text file handling.
- added datafile decorator for ease of use
